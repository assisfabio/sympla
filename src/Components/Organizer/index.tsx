import React, { FunctionComponent, useEffect, useState } from 'react';
import { AppStatus, SectionPropsDefault } from '@Models/index';
import { Avatar, nl2br, Section } from '@UI/index';
import { Paragraph } from '@UI/styles';
import { AvatarContainer, Name, Description, OrganizerContainer } from './styles';

interface thisProps extends SectionPropsDefault {
  description: string,
  name: string,
  pic: string,
}

const Organizer: FunctionComponent<thisProps> =  ({ title, description, name, pic, appStatus }) => {
  const [status, setStatus] = useState<AppStatus | undefined>(appStatus);

  useEffect(() => {
    setStatus(appStatus);
  }, [appStatus]);

  return (
    <Section title={title}>
      <>
        {!status || status === '_LOADING_' ? 'Loading...' : (
          <OrganizerContainer>
            <AvatarContainer>
              <Avatar src={pic} alt={name} />
            </AvatarContainer>
            <Name>{name}</Name>
            <Description>{nl2br(description).map((item, i) => <Paragraph key={i}>{item}</Paragraph>)}</Description>
          </OrganizerContainer>
        )}
      </>
    </Section>
  );
};

export default Organizer;