import React, { FunctionComponent, useEffect, useState } from 'react';

import { Star, RatingContainer, RatingNumber } from './styles';

type thisProps = {
  rating: number,
  stars?: number,
  showNumber?: true
}
const RatingComponent: FunctionComponent<thisProps> = ({ rating, stars, showNumber }) => {
  const [value, setValue] = useState<number>(rating);

  useEffect(() => {
    setValue(rating);
  }, [rating]);

  return (
    <RatingContainer>
      {showNumber && (
        <RatingNumber>{new Intl.NumberFormat('pt-BR', { minimumSignificantDigits: 2 }).format(value)}</RatingNumber>
      )}
      {[...Array(stars || 5)].map((item, i) => (
        <Star key={i} height={'30px'} width="30px" active={i + 1 <= value ? 1 : 0} />
      ))}
    </RatingContainer>
  )
};

export default RatingComponent;