import styled from 'styled-components';

export const Comment = styled.div`
  display: grid;
  grid-gap: 5px 10px;
  grid-template-columns: min-content auto;
  grid-template-rows: max-content;
  margin-bottom: 2em;
`;

export const Username = styled.h4`
  font-size: 1em;
  font-weight: 600;
`;

export const AvatarContainer = styled.div`
  grid-row: 1/3;
`;

export const RatingContainer = styled.div`
  font-size: ${props => props.large ? '23px' : '14px'};
`;

export const Text = styled.div`
  grid-column: 1/3;
`;