import styled from 'styled-components';
import { SectionContainer } from '@UI/Section/styles';
import { BrandLogo } from '@UI/Brand/styles';

export const Foot = styled.footer`
  align-items: center;
  background-color: var(--footer-bg);
  color: var(--footer-color);
  display: flex;
  font-size: 12px;
  justify-content: center;
`;

export const FootContainer = styled(SectionContainer)`
  padding: 1.6875rem;
  grid-template-columns: 1fr 1fr 1fr;
  grid-template-rows: auto;
  grid-template-areas: 
    "compradores brand produtores"
    "apps brand produtores"
    "copyright copyright copyright";
  
  @media(max-width: 640px) {
    grid-template-columns: auto;
    grid-template-areas: 
      "brand"
      "compradores"
      "produtores"
      "apps"
      "copyright";
  }
`;

export const BrandContainer = styled.div`
  align-items: center;
  color: var(--grey-lighter);
  display: flex;
  flex-direction: column;
  font-size: 12px;
  font-weight: 600;
  grid-area: brand;
  justify-self: center;
  text-transform: uppercase;
  ${BrandLogo} {
    margin: 0 0 30px;
  }
`;

export const Copyright = styled.div`
  color: var(--grey-lighter);
  font-size: 10px;
  font-weight: 300;
  grid-area: copyright;
  justify-self: center;
  line-height: 1.5em;
  text-align: center;
  text-transform: uppercase;
`;

export const Menu = styled.dl`
  justify-self: ${props => props.right ? 'end' : 'start'};
  text-align: ${props => props.right ? 'right' : 'left'};
  grid-area: ${props => props.area};
  dt {
    margin-bottom: 20px;
    text-transform: uppercase;
  }
  dd {
    color: var(--grey-lightest);
    margin-bottom: 15px;
    a {
      color: inherit;
      text-decoration: none;
    }
  }
  @media(max-width: 640px) {
    justify-self: center;
    text-align: center;
  }
`;