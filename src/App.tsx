import React, {
  FunctionComponent,
  ReactComponentElement,
  useEffect,
  useState,
} from "react";
import {
  Comments,
  Description,
  EventInfo,
  Footer,
  Header,
  Organizer,
} from "@Components/index";

import { Main } from "@UI/styles";

import { AppStatus, EventProps } from "@Models/index";
import { EventServices } from "./Services";
const services: EventServices = new EventServices();

const App: FunctionComponent = (): ReactComponentElement<FunctionComponent> => {
  const [status, setStatus] = useState<AppStatus>(null!);
  const [event, setEvent] = useState<EventProps>(null!);

  const getData: Function = (): void => {
    setStatus(AppStatus.LOADING);

    services.event(1).then((response: EventProps | null): void => {
      if (response) {
        console.log(response);
        setEvent(response);
        setStatus(AppStatus.STARTED);
      } else {
        setStatus(AppStatus.ERROR);
      }
    });
  };

  /**
   * Inicializando aplicação
   */
  useEffect(() => {
    getData();
  }, []);

  return (
    <Main>
      <Header />
      <article>
        <header>
          <EventInfo event={event} appStatus={status} />
        </header>
        <Description
          title="Descrição"
          text={event?.description}
          appStatus={status}
        />
        <Organizer
          title="Organizador"
          description={event?.organizer_description}
          name={event?.organizer_name}
          pic={event?.organizer_profile_pic}
          appStatus={status}
        />
        <Comments
          title="Comentários"
          list={event?.comments}
          appStatus={status}
        />
      </article>
      <Footer />
    </Main>
  );
};

export default App;
