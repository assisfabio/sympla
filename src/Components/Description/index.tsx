import React, { FunctionComponent, useEffect, useState } from 'react';
import { AppStatus, SectionPropsDefault } from '@Models/index';
import { nl2br, Section } from '@UI/index';
import { Paragraph } from '@UI/styles';

interface thisProps extends SectionPropsDefault {
  text: string,
}

const Description: FunctionComponent<thisProps> = ({ title, text, appStatus }) => {
  const [status, setStatus] = useState<AppStatus | undefined>(appStatus);

  useEffect(() => {
    setStatus(appStatus);
  }, [appStatus]);

  return (
    <Section title={title}>
      {!status || status === '_LOADING_' ? (
        'Loading...'
      ) : (
        nl2br(text).map((item, i) => <Paragraph key={i}>{item}</Paragraph>)
      )}
    </Section>
  );
};

export default Description;