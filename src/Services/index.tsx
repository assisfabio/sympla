import axios from 'axios';
import { EventProps } from '@Models/index';

export class EventServices {
  private urlBase: string = 'https://server-api-test-e2403.firebaseapp.com';

  async event(id: number): Promise<EventProps> {
    return axios.get(`${this.urlBase}/event/${id}`)
      .then((response: any) => response?.data)
      .catch(() => null);
  }
}