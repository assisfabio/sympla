import styled from 'styled-components';

export const CustomButton = styled.button`
  border: 0;
  border-radius: 1.875em;
  font-weight: 600;
  letter-spacing: 0.08em;
  padding: 1.1875em 3.125em 1.125em;
  text-transform: uppercase; 
  
  ${props => {
    let result = '';
    let bgColor = '#ccc';
    let color = '#fff';
    let shadowColor = 'rgba(100, 100, 100, 0.5)'

    if (props.success) {
      bgColor = 'var(--success-bg)';
      color = 'var(--success-color)'
      shadowColor = 'var(--success-shadow)'
    } else if (props.warning) {
      bgColor = 'var(--warning-bg)';
      color = 'var(--warning-color)'
      shadowColor = 'var(--warning-shadow)'
    } else if (props.error) {
      bgColor = 'var(--error-bg)';
      color = 'var(--error-color)'
      shadowColor = 'var(--error-shadow)'
    }

    result += `font-size: ${props.small ? '8px' : '1rem'};`

    if (props.outline) {
      result += 'background-color: transparent;'
      result += `border: 1px solid ${bgColor};`
      result += `box-shadow: 1px 1px 1px ${shadowColor};`
      result += `color: ${bgColor};`
      result += `text-shadow: 1px 1px 1px ${shadowColor};`
    } else {
      result += `background-color: ${bgColor};`
      result += 'border: 0;'
      result += `box-shadow: 0 6px 15px 0 ${shadowColor};`
      result += `color: ${color};`
    }

    return result;
  }}

  transition: filter .3s ease-in-out;

  &:hover {
    filter: contrast(2);
  }
`;