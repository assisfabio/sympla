import styled from 'styled-components';

export const Container = styled.div`
  display: grid;
  grid-gap: 5px 0;
  grid-template-columns: min-content 1fr;
  grid-template-rows: min-content 1fr;
`;

export const Icon = styled.div`
  grid-column: 1;
  grid-row: 1 / 3;
  margin-right: 15px;
  width: auto;
`;
export const Title = styled.div`
  font-size: 12px;
  font-weight: 600;
`;

export const Text = styled.div`
  font-size: 12px;
  line-height: 1.5em;
`;
