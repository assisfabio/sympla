import styled from 'styled-components';

export const Main = styled.main`
  display: flex;
  flex-direction: column;
`;

export const BlurredBg = styled.div`
  height: 100%;
  left: 0;
  overflow: hidden;
  position: absolute;
  top: 0;
  width: 100%;
  z-index: -1;
  &:before {
    ${props => props.fixed && 'background-attachment: fixed;'}
    content: '';
    background-image: url(${props => props.image});
    background-size: cover;
    background-position: center;
    filter: blur(15px);
    height: 100%;
    left: -10%;
    position: absolute;
    top: -10%;
    transform: scale(1.3);
    width: 100%;
    z-index: -1;
  }
`;

export const Paragraph = styled.p`
  font-size: inherit;
  font-weight: 300;
  line-height: 1.5em;
  margin-bottom: 1em;
  &:last-child {
    margin-bottom: 0;
  }
`;