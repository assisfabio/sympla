import styled from 'styled-components';
import { ReactComponent as DataIcon } from './data.svg';
import { ReactComponent as LocalIcon } from './local.svg';
import { Section, SectionContainer } from '@UI/Section/styles';
import { Paragraph } from '@UI/styles';
import { Button } from '@UI/';

export const EventContainer = styled.div`
  color: #fff;
  margin-bottom: 6em;
  padding: 1.6875rem;
  position: relative;
  text-shadow: 1px 1px 1px rgba(100, 100, 100, .3);
`;
export const SectionEvent = styled(Section)`
  padding-top: 1.6875rem;
`;
export const SectionEventContainer = styled(SectionContainer)`
  padding: 0;
  grid-gap: 10px 1.6875rem;
  @media(max-width: 960px) {
  }
`;
export const EventTitle = styled.h1`
  color: inherit;
  font-size: 2.5em;
  font-weight: 900;
  line-height: 0.9;
  max-width: 360px;
  @media(max-width: 960px) {
    max-width: initial;
  }
`;

export const EventData = styled.div`
  display: grid;
  grid-gap: 10px;
  grid-template-columns: 1fr 1fr 1fr;
  grid-template-rows: min-content auto;
  margin: 0 20px;
  @media(max-width: 960px) {
    margin: auto;
  }
`;
export const EventCollection = styled.div`
  color: inherit;
  font-size: 1em;
  font-weight: 300;
  grid-column: 1/3;
  @media(max-width: 960px) {
    grid-column: auto;
  }
`;
export const Subtitle = styled(Paragraph)`
  font-size: 1em;
  margin-bottom: 1em;
  grid-column: 1/4;
`;

const IconDefaults = `
  fill: currentColor;
  height: 18px;
`;
export const Data = styled(DataIcon)`
  ${IconDefaults}
`;
export const Local = styled(LocalIcon)`
  ${IconDefaults}
`;

export const CustomButton = styled(Button)`
  position: absolute;
  top: 100%;
  transform: translate(0%, -50%);
`;