import React, { FunctionComponent } from 'react';
import { Brand, Button } from '@UI/index';
import {
  Foot,
  FootContainer,
  BrandContainer,
  Copyright,
  Menu,
} from './styles';

const Footer: FunctionComponent = () => {
  return (
    <Foot>
      <FootContainer>
        <BrandContainer>
          <span>Powered by</span>
          <Brand model="custom" width={103} />
          <Button outline small>
            Publique seu evento
          </Button>
        </BrandContainer>
        <Menu area="compradores" left>
          <dt>Para compradores</dt>
          <dd><a href="#name_here">Ver mais eventos</a></dd>
          <dd><a href="#name_here">Denuncie esse evento</a></dd>
          <dd><a href="#name_here">Termos e políticas</a></dd>
        </Menu>
        <Menu area="produtores" right>
          <dt>Para produtores</dt>
          <dd><a href="#name_here">Como funciona</a></dd>
          <dd><a href="#name_here">Soluções</a></dd>
          <dd><a href="#name_here">Serviços</a></dd>
          <dd><a href="#name_here">Preços</a></dd>
          <dd><a href="#name_here">Cases</a></dd>
          <dd><a href="#name_here">API</a></dd>
        </Menu>
        
        <Menu area="apps" left>
          <dt>Compre pelo app</dt>
          <dd><a href="#name_here">Android</a></dd>
          <dd><a href="#name_here">iOS</a></dd>
        </Menu>
        <Copyright>
          <p>
            Copyright {new Date().getFullYear()}<br />
            Sympla Internet Soluções S.A.
          </p>
        </Copyright>
      </FootContainer>
    </Foot>
  );
};

export default Footer;
