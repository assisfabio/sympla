import styled from 'styled-components';

import { ReactComponent as StarIcon } from './star.svg';

export const RatingContainer = styled.div`
`;

export const RatingNumber = styled.span`
  font-size: 1.5em;
`;

export const Star = styled(StarIcon)`
  fill: ${props => props.active ? '#ffc600' : '#999'};
  height: 1em;
  width: 1.3em;
`;