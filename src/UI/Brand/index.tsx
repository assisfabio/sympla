import React, { useMemo, FunctionComponent } from 'react';
import { BrandLogo } from './styles';

type BrandProps = {
  height?: number,
  link?: string,
  model?: string,
  width?: number,
};

const Brand: FunctionComponent<BrandProps> = ({
    height,
    link,
    model,
    width
}) => {
  const MemoLogo = useMemo(
    () => <BrandLogo height={height} model={model} width={width} />,
    [height, model, width]
  );

  if (link) {
    return <a href={link}>{MemoLogo}</a>
  }
  return <>{MemoLogo}</>
};

export default Brand;