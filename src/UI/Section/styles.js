import styled from 'styled-components';

export const Section = styled.section`
  align-items: center;
  display: flex;
  justify-content: center;
`;
export const SectionContainer = styled.div`
  display: grid;
  grid-template-columns: 40% auto;
  grid-gap: 1.6875rem;
  max-width: 1138px;
  padding: 0 1.6875rem 6rem;
  width: 100%;
  @media(max-width: 960px) {
    grid-template-columns: auto;
  }
`;

export const SectionTitle = styled.div`
`;
export const Title = styled.h2`
  font-size: 2.5rem;
  font-weight: 300;
  margin-bottom: .5em;
`;
export const Subtitle = styled.div`
`;

export const SectionText = styled.div`
  font-size: 1rem;
`;