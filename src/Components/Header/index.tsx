import React, { FunctionComponent } from 'react'; // importing FunctionComponent
import { Brand } from '../../UI';
import {
  Head,
  HeadContainer,
} from './styles';

const Header: FunctionComponent = () => {
  return <Head>
    <HeadContainer>
      <Brand />
    </HeadContainer>
  </Head>
};

export default Header;