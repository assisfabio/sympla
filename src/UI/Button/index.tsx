import React, { ButtonHTMLAttributes, FunctionComponent } from 'react';

import {
  CustomButton
} from './styles';

interface ButtonConfigProps extends ButtonHTMLAttributes<any> {
  outline?: boolean,
  warning?: boolean,
  success?: boolean,
  error?: boolean,
  block?: boolean,
  loading?: boolean,
  small?: boolean,
};

const Button: FunctionComponent<ButtonConfigProps> = (props: ButtonConfigProps) => {
  const { children } = props;
  return (
    <CustomButton {...props}>{children}</CustomButton>
  )
};

export default Button;