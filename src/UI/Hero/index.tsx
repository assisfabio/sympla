import React, { FunctionComponent } from 'react';
import { BannerBg, BannerImg } from './styles';

type thisProps = {
  src: string,
  alt?: string,
  model?: 'small'
};

const Hero: FunctionComponent<thisProps> = ({ alt, src, model }) => {
  return (
    <>
      <BannerBg image={src} />
      <BannerImg srcSet={src} alt="" />
    </>
  );
};

export default Hero;