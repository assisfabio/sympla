import styled from 'styled-components';

export const OrganizerContainer = styled.div`
  display: grid;
  grid-template-columns: auto auto;
  grid-template-rows: auto auto;
  grid-gap: 1rem 1.5rem;
`;

export const AvatarContainer = styled.div`
  grid-row: 1/3;
`;

export const Name = styled.h3`
  align-items: center;
  display: flex;
  font-size: 1.5em;
  font-weight: 500;
  min-height: 60px;
`;

export const Description = styled.div`
  display: block;
`;
