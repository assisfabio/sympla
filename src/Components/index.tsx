import Comments from './Comments';
import Description from './Description';
import EventInfo from './EventInfo';
import Footer from './Footer';
import Header from './Header';
import Organizer from './Organizer';

export {
  Comments,
  Description,
  EventInfo,
  Footer,
  Header,
  Organizer,
}