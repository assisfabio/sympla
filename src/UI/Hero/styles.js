import styled from 'styled-components';
import { BlurredBg } from '@UI/styles';

export const BannerBg = styled(BlurredBg)`
  @media (prefers-color-scheme: dark) {
    &:before {
      filter: brightness(0.3) grayscale(0.5);
    }
  }
`;

export const BannerImg = styled.img`
  box-shadow: 0 10px 30px 0 rgba(100, 100, 100, 0.5);
  display: block;
  height: auto;
  max-width: 100%;
  margin: 0 auto;
`;