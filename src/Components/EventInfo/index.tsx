import React, { FunctionComponent, useEffect, useState } from "react";
import { AppStatus, EventProps } from "@Models/index";
import { Avatar, breakInNWords, Hero, InfoPills } from "@UI/index";
import {
  EventContainer,
  SectionEvent as Section,
  SectionEventContainer as SectionContainer,
  EventTitle,
  EventData,
  Subtitle,
  EventCollection,
  Data as DataIcon,
  Local as LocalIcon,
  CustomButton as Button,
} from "./styles";

type thisProps = {
  appStatus: AppStatus;
  event: EventProps;
};

const EventInfo: FunctionComponent<thisProps> = ({ appStatus, event }) => {
  const [status, setStatus] = useState<AppStatus | undefined>(appStatus);
  const [eventInfo, setEventInfo] = useState<EventProps>(event);

  const getBreakedWords = (phrase: string, point: number): any => {
    const result = breakInNWords(phrase, point);
    return (
      <>
        {result.start}
        <br />
        {result.end}
      </>
    );
  };

  useEffect(() => {
    setEventInfo(event);
  }, [event]);

  useEffect(() => {
    setStatus(appStatus);
  }, [appStatus]);

  return (
    <>
      {!status || status === "_LOADING_" ? (
        "Loading..."
      ) : (
        <EventContainer>
          <Hero src={event.banner_url} />

          <Section>
            <SectionContainer>
              <EventCollection>{eventInfo.collection}</EventCollection>
              <EventTitle>{eventInfo.title}</EventTitle>
              <EventData>
                <Subtitle>{eventInfo.subtitle}</Subtitle>
                <InfoPills
                  Label="Data"
                  Value={() => getBreakedWords(eventInfo.date, 3)}
                  Icon={() => <DataIcon />}
                />
                <InfoPills
                  Label="Local"
                  Value={() => getBreakedWords(eventInfo.location, 2)}
                  Icon={() => <LocalIcon />}
                />
                <InfoPills
                  Label="Organizador"
                  Value={eventInfo.organizer_name}
                  Icon={() => (
                    <Avatar
                      src={eventInfo.organizer_profile_pic}
                      alt={eventInfo.organizer_name}
                      model={"small"}
                    />
                  )}
                />
              </EventData>
              <Button warning>Comprar ingressos</Button>
            </SectionContainer>
          </Section>
        </EventContainer>
      )}
    </>
  );
};

export default EventInfo;
