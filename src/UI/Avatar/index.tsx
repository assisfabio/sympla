import React, { FunctionComponent } from 'react';
import { Avatar as AvatarImg, FaultBack } from './styles';

type thisProps = {
  src: string,
  alt?: string,
  model?: 'small'
};

const Avatar: FunctionComponent<thisProps> = ({ alt, src, model }) => {
  return src ? (
    <AvatarImg srcSet={src} alt={alt} model={model} />
  ) : (
    <FaultBack model={model} />
  );
};

export default Avatar;