import React, { FunctionComponent } from 'react';
import { Container, Icon as IconContainer, Title, Text } from './styles';

type thisProps = {
  Label: string,
  Value: any,
  Icon?: any,
};

const InfoPills: FunctionComponent<thisProps> = ({ Label, Value, Icon }) => {
  console.log(typeof Value);
  return (
    <Container>
      <IconContainer>
        <Icon />
      </IconContainer>
      <Title>{Label}</Title>
      <Text>{typeof Value === 'function' ? <Value /> : Value}</Text>
    </Container>
  );
};

export default InfoPills;