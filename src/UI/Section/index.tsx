import React, { FunctionComponent } from 'react';
import { SectionPropsDefault } from '@Models/index';

import {
  Section,
  SectionContainer,
  SectionTitle,
  SectionText,
  Title,
  Subtitle,
} from './styles';

const SectionComponent: FunctionComponent<SectionPropsDefault> = ({ title, children, SubtitleComp }) => {
  return (
    <Section>
      <SectionContainer>
        <SectionTitle>
          <Title>{title}</Title>
          {SubtitleComp && (<Subtitle>{SubtitleComp}</Subtitle>)}
        </SectionTitle>
        <SectionText>{ children }</SectionText>
      </SectionContainer>
    </Section>
  )
};

export default SectionComponent;