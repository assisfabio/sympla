import styled from 'styled-components';
import { ReactComponent as FaultBackImg } from './faultback.svg';

const DefaultStyles = `
  border-radius: 50%;
  display: inline-block;
  height: auto;
`

export const Avatar = styled.img`
  ${DefaultStyles}
  width: ${props => {
    if (props.model === 'small') {
      return '48px';
    } else {
      return '80px';
    }
  }}
`;

export const FaultBack = styled(FaultBackImg)`
  ${DefaultStyles}
  width: ${props => {
    if (props.model === 'small') {
      return '48px';
    } else {
      return '80px';
    }
  }}
`;