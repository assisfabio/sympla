import styled from 'styled-components';
import { SectionContainer } from '@UI/Section/styles';

export const Head = styled.header`
  align-items: center;
  background-color: var(--header-bg);
  display: flex;
  justify-content: center;
`;

export const HeadContainer = styled(SectionContainer)`
  align-items: center;
  display: flex;
  justify-content: center;
  min-height: 88px;
  padding: 1.6875rem;
`;