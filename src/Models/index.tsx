
export enum AppStatus {
  LOADING = '_LOADING_',
  STARTED = '_STARTED_',
  ERROR = '_ERROR_',
};

export type CommentProps = {
  profile_pic: string,
  rating: number,
  text: string,
  username: string,
};

export type TicketProps = {
  fee: number,
  id: number,
  installments_max: number,
  installments_price: number,
  price: number,
  title: string,
};

export type EventProps = {
  banner_url: string,
  collection: string,
  comments: Array<CommentProps>,
  date: string,
  description: string,
  location: string,
  organizer_description: string,
  organizer_name: string,
  organizer_profile_pic: string,
  subtitle: string,
  tickets: Iterable<TicketProps>,
  title: string,
}

export type SectionPropsDefault = {
  title: string,
  appStatus?: AppStatus,
  SubtitleComp?: any,
};