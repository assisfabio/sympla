import React, { FunctionComponent, useEffect, useMemo, useState } from 'react';
import { AppStatus, CommentProps, SectionPropsDefault } from '@Models/index';
import { Avatar, Rating, nl2br, Section } from '@UI/index';
import { Paragraph } from '@UI/styles';
import { Comment, AvatarContainer, RatingContainer, Username, Text } from './styles';

interface thisProps extends SectionPropsDefault {
  list: Array<CommentProps>,
}

const Comments: FunctionComponent<thisProps> =  ({ title, list, appStatus }) => {
  const [status, setStatus] = useState<AppStatus | undefined>(appStatus);

  useEffect(() => {
    setStatus(appStatus);
  }, [appStatus]);

  const MemoComments = useMemo(() => {
    return (
      <>
        {list?.map((item: CommentProps, i: number) => { 
          return (
            <Comment key={i}>
              <AvatarContainer>
                <Avatar src={item.profile_pic} alt={item.username} model={'small'} />
              </AvatarContainer>
              <Username>{item.username}</Username>
              <RatingContainer>
                <Rating rating={item.rating} />
              </RatingContainer>
              <Text>
                {nl2br(item.text).map((item, i) => <Paragraph key={i}>{item}</Paragraph>)}
              </Text>
            </Comment>
          )
        })}
      </>
    );
  }, [list]);

  const MemoRating = useMemo(() => {
    const rating = list?.reduce((total, current) => total + Number(current.rating), 0) / list?.length;
    return (
      <RatingContainer large>
        <Rating rating={!isNaN(rating) ? rating : 0} showNumber />
      </RatingContainer>
    );
  }, [list]);

  return (
    <Section title={title} SubtitleComp={MemoRating}>
      <>
        {!status || status === '_LOADING_' ? 'Loading...' : (MemoComments)}
      </>
    </Section>
  );
};

export default Comments;