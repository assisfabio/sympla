import Avatar from './Avatar';
import Button from './Button';
import Brand from './Brand';
import Hero from './Hero';
import InfoPills from './InfoPills';
import Rating from './Rating';
import Section from './Section';

export {
  Avatar,
  Button,
  Brand,
  Hero,
  InfoPills,
  Rating,
  Section,
};

/**
 * Converte uma string (phrase) em um objeto, seprando o inicio
 * do fim, sendo o fim cálculado pelo parâmetro (lasts)
 * @param phrase <string> Texto que será dividido
 * @param lasts <number> Número de palavras no texto final
 */
export const breakInNWords = (phrase: string, lasts: number): any => {
  const splited = phrase.split(/[\s,]+/);
  const result = { start: '', end: '' };

  splited.forEach((word: string, i: number): void => {
    if (i < splited.length - lasts) {
      result.start += `${word} `;
    } else {
      result.end += `${word} `;
    }
  })

  return result;
}

/**
 * Converte um texto com \r\n | \r | \n em um array de strings
 * Verifique no codigo a possibilidade de nós vazios no Array resultante
 * @param text <string> Texto a ser decomposto
 */
export const nl2br = (text: string): Array<string> => {
  return text?.split(/(?:\r\n|\r|\n)/g);
};
