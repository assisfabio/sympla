import styled from 'styled-components';
import { ReactComponent as SymplaLogo } from './sympla-logo.svg';

export const BrandLogo = styled(SymplaLogo)`
  fill: var(--brand-color);
  ${props => {
    let result = '';
    if (props.model === 'custom') {
      result += `height: ${props.height ? props.height + 'px' : 'auto'};`
      result += `width: ${props.width ? props.width + 'px' : 'auto'};`
    } else {
      result += `height: auto;`
      result += `width: 128px;`
    }
    return result;
  }}
`;